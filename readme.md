# Abstract
Las imágenes de esta carpeta requieren estar pinneadas en IPFS por al menos un nodo (si es posible en un Cluster) ya que son las mínimas necesarias para el correcto funcionamiento de la app de dPoint y del contenido que allí se muestra

# Imágenes contenidas

## Iconos y Logotipos de Detailorg 
> SVG
- <img src="assets/logo_detailorg.svg" width="34"> Logotipo: ***QmabPJF6VF6pZRWyBLSriK1K5v2YZ3h3Qx3r6Tbf5tkagn***
- <img src="assets/logo_detailorg_simplificado.svg" width="34">  Logotipo simplificado: ***Qma7PUQqyTnCAdckpYNE9B6Mcdcptc7HgQCJHHBRuiDfqW***
- <img src="assets/logo_detailorg_completo.svg" width="72"> Logotipo + nombre: ***Qmd3u9gLmUBfrLC2gYdosXAYMgcu3PC35jhrAHC4FkNDx1***
- <img src="assets/logo_detailorg_completo_gris.svg" width="72"> Logotipo + nombre en gris: ***QmW3ZTv8SFTXYviePtGEZPedr4rGaMpJmPjcTJQhqyUG53***
- <img src="assets/logo_detailorg_powered.svg" width="72"> Logotipo + nombre + Powered: ***QmVE7eMKuvdkaWYtrSGWa9uCAjEvy3GgHUb4VoBcuWi3Se***
- <img src="assets/logo_detailorg_powered_gris.svg" width="72"> Logotipo + nombre + Powered en gris: ***QmXmZEhvEoAmgUVMykr3mXPd97uhvgvMHgY4NmZNsZFBME***

> PNG
- <img src="assets/logo_detailorg.png" width="34"> ***QmPn5NCBZ859omnhPBobyo48P7SM7r8aAc3XKKmVLmjKW3***: logotipo
- <img src="assets/logo_detailorg_simplificado.png" width="34"> ***QmSVMnQcLNCfYxm3aBUX7UcZjDmabWcesk1CvRjihJX64m***: logotipo simplificado
- <img src="assets/logo_detailorg_completo.png" width="72"> ***QmQkRx3d3M9u3SYLikutJYHHRqYHnni6JfvMGhAzkXc5uV***: logotipo + nombre
- <img src="assets/logo_detailorg_completo_gris.png" width="72"> ***QmNnGajLCRoCqcpX4Wgd7cgcHSeLaCNXo5fVfeQxzDzBVP***: logotipo + nombre en gris
- <img src="assets/logo_detailorg_powered_gris.png" width="72"> ***QmcAvTxAvZnAEzcd6nCBYHguDJ3azWwKBGx5PSRaorqHgs***: logotipo + nombre + Powered en gris
- <img src="assets/QR_dpoint.png" width="72"> ***QmWU1qj7PZW1L4wfNWZCJZLzx5LjKa9FsiqXqyCpaWYBzC***: link a dpoint.detailorg.com con fondo transparente
- <img src="assets/QR_dpoint_fondo_blanco.png" width="72"> ***QmQjx5J1VwcJxqSg5C9BePZ1TxfSsTcXouMD1xDKjhjCye***:  link a dpoint.detailorg.com con fondo blanco

> JPG
- <img src="assets/logo_detailorg.jpg" width="34"> ***QmcoWbfRtanAKd9VXS9Vi7s1F6CGWzJfXxx21LtmeLXQWY***: logotipo
- <img src="assets/logo_detailorg_simplificado.jpg" width="34"> ***QmexpXBpJNQA4wJnda2RjDN9xJ67bZsutpfZv8Q2r9xFxU***: logotipo simplificado
- <img src="assets/logo_detailorg_completo.jpg" width="72"> ***QmQSH4QHD79wj3KUARxJiX8HinRdrnVYehhX9c1ropQvcG***: logotipo + nombre
- <img src="assets/logo_detailorg_completo_gris.jpg" width="72"> ***QmPTHT9KN1TmrXAhQuL3dkHVfzpMnw1FRuhzaxBWfMfrRu***: logotipo + nombre en gris
- <img src="assets/logo_detailorg_powered_gris.jpg" width="72"> ***Qma9ysu3iTXbw1edfW8JPocDZyQCf7A78PdY9amaYZfzqk***: logotipo + nombre + Powered en gris
- <img src="assets/blockchain_background.jpg" width="72"> ***QmSie976dLQfuN1FhUSuc6NWuyEYGCjmUrjxTbaNXqAVSA***: Imágen de Fondo
___________________________

## Iconos de PV (puntos de vista) de dPoint
> SVG
- <img src="assets/icon_pv0.svg" width="34"> PV0:  ***Qmd9sJujd2ZwJATyvUppDQeFa37jYUof3yJJUr8Mpf2R71***
- <img src="assets/icon_pv1.svg" width="34"> PV1:  ***QmQmBPiU8EZem3xkx5JdAWUumasoMQWUcxUq6pbxv8LsEv***
- <img src="assets/icon_pv2.svg" width="34"> PV2:  ***Qmb4m8dKKQCNPnvtrY8brabE7CkCUQtmD45w4hgU7GYnvJ***
- <img src="assets/icon_pv3.svg" width="34"> PV3:  ***QmVdDMwzAFr6gunC5v4FpGYpc96QqSqQALW9M1wwXiEdWH***
- <img src="assets/icon_pv4.svg" width="34"> PV4:  ***QmQevp48CxfGYSdvHFtmBEo4AoHUUf3Z94mYTAi5QDJLZj***
- <img src="assets/icon_pv5.svg" width="34"> PV5:  ***QmP6EkMKhNKhU6tnrWrMbGroWssByLcpJvtW142Jv7dxMw***
- <img src="assets/icon_pv6.svg" width="34"> PV6:  ***QmV5Kb45bW2sp2nEZFotZXiJSTN3bdyqNgiZr7uz5qrpwp***
- <img src="assets/icon_pv7.svg" width="34"> PV7:  ***QmdynhnTmh5RrZaa9Ukuw4sMHDP1pY5NKSCJwWsLdWfExf***
- <img src="assets/icon_pv8.svg" width="34"> PV8:  ***QmTAjExBCGxpQvuJNnSfVs6hSrRiEbchQM5xt4XufshJj1***
- <img src="assets/icon_pv9.svg" width="34"> PV9:  ***QmbGvRyVbcvAvRyxAfipf6qj88dax3c4sdxqLuS1afvFKH***

__________________

## Imágenes en los tamaños especiales del pkpass
Las imágenes del pkpass se distribuyen de la siguiente manera (esto es el genérico)

<img src="assets/infografia_pkpass_generic_2x.png" width=800>

> PNG
- <img src="assets/pkpass_icon.png" width="34"> Icon: ***QmXkjGkxALJkD9yUMeRHumQiisv8fGH8FW82vuh8MNmACg:*** The icon (icon.png) is displayed when a pass is shown on the lock screen and by apps such as Mail when showing a pass attached to an email. The icon should measure 29 x 29 pk.
- <img src="assets/pkpass_logo.png" width="72"> Logo: ***QmNt1gwteWLQaHnuQ7aVzVMp1raiqUxcgZAuhMCHFHrXUj:*** The logo image (logo.png) is displayed in the top left corner of the pass, next to the logo text. The allotted space is 160 x 50 px; in most cases it should be narrower.
- <img src="assets/pkpass_background.png" width="72"> Background: ***QmYx56ZY5maHoKGssWUYDFc75tQrg5YteEyEpSpEM1Jx9H:*** The background image (background.png) is displayed behind the entire front of the pass. The expected dimensions are 180 x 220 px. The image is cropped slightly on all sides and blurred. Depending on the image, you can often provide an image at a smaller size and let it be scaled up, because the blur effect hides details. This lets you reduce the file size without a noticeable difference in the pass.
- <img src="assets/pkpass_strip.png" width="72"> Strip: ***QmTwEJfqGRjuDoo6DPfn4vDxRcBGxQYKysa68EyU96WSLW:*** The strip image (strip.png) is displayed behind the primary fields. The allotted space is 320 x 110 px
- <img src="assets/pkpass_footer.png" width="72"> Footer: ***QmWXEZWXcPRXnjJYbA1LHx3iG9Wa9tSkTFMyQzqfJpwLsn:*** The footer image (footer.png) is displayed near the barcode. The allotted space is 286 x 15 px.
- <img src="assets/pkpass_thumbnail.png" width="72"> Thumbnail: ***QmSs2keRyfstQmCPwDKgY6aqKtuJJqHDMAHhjzkqFoiuGP:*** The thumbnail image (thumbnail.png) displayed next to the fields on the front of the pass. The allotted space is 90 x 90 px. The aspect ratio should be in the range of 2:3 to 3:2, otherwise the image is cropped.